<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appointments', function (Blueprint $table) {
            //
            $table->dropForeign(['patient_id']);
            $table->boolean("state")->default(0);
        });
        Schema::table('appointments', function (Blueprint $table) {
            $table->integer("patient_id")->unsigned()->nullable()->change();
            $table->foreign("patient_id")->references("id")->on("users")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appointments', function (Blueprint $table) {
            //

        });
    }
}
