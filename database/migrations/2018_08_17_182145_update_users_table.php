<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->date("birthday");
            $table->string("surname");
            $table->string("parent_name");
            $table->bigInteger("phone_number");
            $table->integer("gender");
            $table->string("profession")->nullable();
            $table->string("experience")->nullable();
            $table->string("certificate")->nullable();
            $table->string("diploma")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(["birthday", "surname", "parent_name", "phone_number", "gender", "profession", "experience",
                "certificate", "diploma"]);
        });
    }
}
