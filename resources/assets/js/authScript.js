let myApp =  {
    CurrentInformation : {},
    Registration : {
        loading : false,
        submitting : false,
        form : $(".registration-form"),
        reader : null,
        eventBind : function() {
            $("input, select").on("focusout", (event)=> {
                if($(event.target).val() === ""){
                    return
                }
                this.sendRequest(event.target);
            });
            $(".submit-ajax").on("click", (e) => {
                e.preventDefault();
                myApp.Registration.submitAjax();
            });
            $("input.file").on("change", (event) => {
                if(event.target.files){
                    console.log(event.target);
                    this.reader = new FileReader();
                    this.reader.onload = function (e) {
                        $("#profile-picture").attr("src", e.target.result)
                    };
                    this.reader.readAsDataURL(event.target.files[0]);
                }
            });
            $(".checkbox").not(".remember").on("change", (event) => {
                this.sendRequest(event.target);
            });
            $("#role").change( () => {
                if($("#role").val() === "2"){
                    $(".doctor-role-inputs").removeClass("d-none")
                } else{
                    $(".doctor-role-inputs").addClass("d-none");
                }
            });
            $(".submit-button").on("click", (event) => {
                event.preventDefault();
                this.submitForm();
            });
            $(".delete-link").on("click", function(){
                myApp.Registration.deletePost(this)
            });
        },
        deletePost : function(link){
            let id;
            let image;
            let type;
            image = $(link).siblings("img");
            id = image.attr("identificator");
            type = image.attr("type");
            $.ajax({
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "/deletepost",
                data: {
                    _method: "DELETE",
                    id: id,
                    type: type,
                },
                success: function (data) {
                    $(link).parent().remove();
                    alert(data);
                },
                error: function(data){
                    console.log(data);
                }
            });
        },
        submitAjax: function(){
            let errMessages;
            let data;
            myApp.Registration.getIdAndUri();
            data = new FormData(document.getElementById(this.id));
            data.append("done", "done");
            console.dir(data.getAll("diploma"));
            for(let i = 0; i < document.getElementById("diplomas").files.length; i ++){
                data.append("diploma[]", document.getElementById("diplomas").files[i])
            }
            for(let i = 0; i < document.getElementById("certificate").files.length; i ++){
                data.append("certificate[]", document.getElementById("certificate").files[i])
            }
            $.ajax({
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "/" + this.uri,
                processData: false,
                contentType: false,
                data: data,
                success: function (data) {
                    alert(data);
                },
                error:function (data) {
                    console.log(data);
                    errMessages = data.responseJSON.errors;
                    myApp.Registration.showErrorMessages(errMessages);
                }
            });
        },
        getIdAndUri : function() {
            this.uri;
            this.id;
            if(location.pathname.replace(/\//g,'') !== "passwordreset" && location.pathname.replace(/\//g,'').length < 15){
                this.uri = location.pathname.replace(/\//g,'');
                this.id = this.uri;
            } else if(location.pathname.length > 15){
                this.uri = "password/reset";
                this.id = "new-password"
            }else {
                this.uri = "password/email";
                this.id = "password-reset";
            }
        },
        sendRequest : function (thisInput) {
            let errMessages;
            myApp.Registration.getIdAndUri();
            $.ajax({
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "/" + this.uri,
                data: $(`#${this.id} :input[name!=done]`).serialize(),
                success: function (data) {
                    if (data.errors !== undefined) {
                        errMessages = data.errors;
                    }
                },
                error:function (data) {
                    if(myApp.Registration.submitting){
                        return;
                    }
                    myApp.Registration.errorMessageHandler(data, thisInput)
                }
            });
        },
        errorMessageHandler : function(data, thisInput){
            console.log(data);
            errMessages = data.responseJSON.errors;
            console.log(errMessages);
            if ($(thisInput).attr("name") !== "Terms_and_Conditions"){

                myApp.Registration.showValidationMessages(thisInput, errMessages);
            } else {
                myApp.Registration.showTermsValidationMessage($(thisInput).parent(), errMessages);
            }
        },
        removeConfirmationMessage : function(errMessages){
            let thisInput = $("#password");
            if(errMessages[thisInput.attr("name")] === undefined || $.inArray("The password confirmation does not match.", errMessages[thisInput.attr("name")]) === -1){
                let removeMessage = $("#password_confirmation");
                removeMessage.nextUntil(".material-placeholder").slideUp("normal", function(){$(this).remove(".invalid")});
                removeMessage.parent().removeClass("invalid");
            }
        },
        showValidationMessages : (thisInput, errMessages) => {
            let changeInput = false;
            myApp.Registration.removeConfirmationMessage(errMessages);
            $(thisInput).nextUntil(".material-placeholder").slideUp("normal", function(){$(this).remove()});
            if(thisInput.name === "password_confirmation"){
                thisInput = document.getElementById("password");
                changeInput = true;
            }
            if (errMessages[thisInput.name] !== undefined) {
                for (let i = 0; i < errMessages[thisInput.name].length; i++) {
                    if(errMessages[thisInput.name][i] === "The password confirmation does not match."){
                        if(errMessages[thisInput.name].length === 1){
                            $(thisInput).parent().removeClass("invalid");
                        }
                        if(!changeInput){
                            continue;
                        }
                        myApp.Registration.showMessage(document.getElementById("password_confirmation"), errMessages[thisInput.name][i]);
                    } else {
                        myApp.Registration.showMessage(thisInput, errMessages[thisInput.name][i]);
                    }
                }
            } else {
                $(thisInput).siblings("span").not(".material-placeholder").slideUp("normal", function(){$(this).remove(".invalid")});
                $(thisInput).parent().removeClass("invalid");
            }
        },
        showMessage : function(thisInput, message){
            $(thisInput).nextUntil(".material-placeholder").remove(".invalid");
            $(thisInput).after(`<span class="invalid d-block" style="display: none!important;">
                        <strong>${message}<br></strong>
                        </span>`
            );
            $(thisInput).parent().addClass("invalid");
            $(thisInput).nextUntil(".material-placeholder").slideDown();
        },
        showTermsValidationMessage : (thisInput, errMessages) => {
            if(errMessages[$(thisInput).children("input").attr("name")] !== undefined){
                $(thisInput).addClass("invalid");
                $(thisInput).children("span").remove();
                for(let i = 0; i < errMessages[$(thisInput).children("input").attr("name")].length; i++){
                    $(thisInput).children("label").after(`<span class="invalid d-block" style="display: none!important;">
                        <strong>${errMessages[$(thisInput).children("input").attr("name")][i]}</strong>
                        </span>`
                    );
                }
                $(thisInput).children("span").slideDown();
            } else {
                $(thisInput).removeClass("invalid");
                $(thisInput).children("span").slideUp("normal", function(){$(this).remove(".invalid")})
            }
        },
        submitForm : function(){
            if(myApp.Registration.loading){
                return;
            }
            myApp.Registration.submitting = true;
            $(".lds-dual-ring-button").removeClass("d-none");
            myApp.Registration.loading = true;
            let errMessages;
            let data;
            this.getIdAndUri();
            data = new FormData(document.getElementById(this.id));
            data.append("done", "done");
            $.ajax({
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "/" + this.uri,
                processData: false,
                contentType: false,
                data: data,
                success: function (data) {
                    $(".lds-dual-ring-button").addClass("d-none");
                    console.log(data);
                    if(data.message !== undefined){
                        alert(data.message);
                    }
                    $(".alert-danger").slideUp();
                    if(data.status === "success" && data.redirectURL !== undefined){
                        location.replace(data.redirectURL)
                    }
                    myApp.Registration.loading = false;
                    myApp.Registration.submitting = false;
                },
                error:function (data) {
                    $(".lds-dual-ring-button").addClass("d-none");
                    errMessages = data.responseJSON.errors;
                    myApp.Registration.showErrorMessages(errMessages);
                    myApp.Registration.loading = false;
                    myApp.Registration.submitting = false;
                }
            });
        },
        showErrorMessages : function(errMessages){
            $("div.invalid").removeClass("invalid");
            $("span.invalid").remove();
            let thisElement;
            $.each(errMessages, (key, value) => {
                if(key !== "Terms_and_Conditions"){
                    for (let i = 0; i < value.length; i++) {
                        if(value[i] === "The password confirmation does not match."){
                            myApp.Registration.showSubmittedMessages($("#password_confirmation"), value[i])
                        } else if(value[i] === "These credentials do not match our records."){
                            myApp.Registration.showAlertMessage(value[i]);
                        } else {
                            myApp.Registration.showSubmittedMessages($(`#${key}`), value[i])
                        }
                    }
                }
            })
        },
        showSubmittedMessages : function(thisElement, message){
            thisElement.parent().addClass("invalid");
            thisElement.after(`<span class="invalid d-block" style="display: none!important;">
                    <strong>${message}<br></strong>
                    </span>`
            );
            thisElement.nextUntil(".material-placeholder").slideDown();
        },
        showAlertMessage : function(message){
            $("form").before(`
                <div class="alert alert-danger" style="display: none" role="alert">
                    <p class="text-center">${message}</p>
                </div>
            `);
            $(".alert-danger").slideDown();
        }
    },
    init : function () {
        myApp.Registration.eventBind();
    }
};
myApp.init();