myApp = {
    eventBind : function(){
        this.time = null;
        this.date = null;
        this.reader = null;
        this.delay = null;
        $(".toggle-icon").on("click", function() {
            $(this).children().toggleClass("fa-times fa-angle-up")
        });
        $(".confirm-button").on("click", (event) => {
            this.time = $(event.target).parent().parent().siblings("button").text();
            this.date = location.pathname.split("/")[3];
            myApp.confirmAppointment(this.time, this.date, event.target);
        });
        $(".testing").on("click", (event) => {
            event.preventDefault();
            this.times = $("#my-form").serialize();
            this.date = location.pathname.split("/")[2];
            myApp.confirmAppointment(this.times, this.date);
        });
        $(".button-toggle").on("click", function() {
            $(this).toggleClass("btn-chosen");
        });
        $(".search").on("keyup", (event) => {
            if($(event.target).val() === ""){
                return;
            }
            clearTimeout(this.delay);
            this.delay = setTimeout(() => {
                myApp.search(event.target)
            }, 1000);
        });
        $(window).on("click", function() {
            if(!$(this).is(".search-result", ".search-drop", ".searching")){
                $(".search-result").addClass("d-none")
            }
        });
        $(".logout").on("click", () => {
            $(".logout-form").submit();
        });
    },
    search : function(input){
        let searchdrop = $(".search-drop");
        let resultDiv = $(".search-result");
        resultDiv.removeClass("d-none");
        searchdrop.html('<div class="mx-auto d-block py-1 lds-dual-ring"></div>');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url : "/search",
            type : "GET",
            data : {
                name : $(input).val(),
            },
            success: function (data) {
                searchdrop.html("");
                if(data.length > 0){
                    for(let i = 0; i < data.length; i++){
                        searchdrop.append(`
                            <a class="d-block text-teal searching my-2" href="/user/${data[i].id}">${data[i].name + " " + data[i].surname}</a>
                        `);
                    }
                } else {
                    searchdrop.append("<span class='d-block text-teal searching my-2'>0 Result</span>")
                }
            },
            error: function (data) {
                console.log(data);
            }
        })
    },
    confirmAppointment : function(time, date, e = null){
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: location.pathname,
            type: "POST",
            data: {
                date: date,
                time: time,
                _method: "PUT",
            },
            success: function (data) {
                console.log(data);
                alert(data);
                if(e !== null){
                    $(e).parentsUntil(".content").remove();
                }
            },
            error: function (data) {
                console.log(data);
            }
        })
    },
    init: function () {
        this.eventBind();
    },
};
myApp.init();