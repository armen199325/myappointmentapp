@extends("layouts.myLayout")

@section("content")
    <div class="content h-auto my-4">
        <div class="row m-0">
            @if(count($appointments) !== 0)
            @foreach($appointments as $key => $appointment)
            <div class="col-md-10 mx-auto my-3">
                @if($oldWeekDay !== Carbon\Carbon::parse($appointment->date_time)->format("w"))
                <p class="text-muted">
                    {{Carbon\Carbon::parse($appointment->date_time)->format("l")}}
                    @php $oldWeekDay = \Carbon\Carbon::parse($appointment->date_time)->format("w") @endphp
                </p>
                <hr>
                @endif
                <table class="w-100">
                    <tr>
                        <td><i class="fas fa-circle text-warning"></i> {{Carbon\Carbon::parse($appointment->date_time)->format("H:i") . " : " .Carbon\Carbon::parse($appointment->date_time)->addHour(1)->format("H:i")}} </td>
                        <td><a href="{{asset(route("showUser", ["id" => $appointment->patient_id]))}}">{{$appointment->name}}</a> <span class="text-black-50">with You</span></td>
                        <td><a class="toggle-icon" data-toggle="collapse" href="#collapseApp{{$key}}" role="button" aria-expanded="false" aria-controls="collapseExample">Details <i class="fas fa-angle-down"></i></a></td>
                    </tr>
                </table>
                <div class="collapse" id="collapseApp{{$key}}">
                    <div class="row m-0">
                        <div class="col-md-4 d-inline-block my-3">
                            <button class="btn mx-3 d-block align-self-center" data-toggle="modal" data-target="#cancel-modal{{$key}}"><i class="fas fa-trash-alt"></i> Cancel</button>
                        </div>
                        <div class="modal fade" id="cancel-modal{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog border w-50">
                                <div class="modal-content rounded-0">
                                    <form action="{{route("mail")}}" method="post" novalidate>
                                        @csrf
                                        @method("PUT")
                                        <div class="modal-header border-0 justify-content-center align-items-center flex-column">
                                            <h5 class="modal-title" id="exampleModalLabel">Cancel Appointment</h5>
                                        </div>
                                        <div class="modal-body">
                                            <h6 class="text-center">{{$appointment->name}}</h6>
                                            <p class="text-black-50 text-center">{{Carbon\Carbon::parse($appointment->date_time)->format("H:i"). " : " .Carbon\Carbon::parse($appointment->date_time)->addHour(1)->format("H:i")}}</p>
                                            <p>Please Confirm That You Would Like to Cancel This Appointment</p>
                                            <div class="form-group position-relative">
                                                <input type="text" name="message" class="form-control rounded-0 material-input" required>
                                                <span class="material-placeholder">Add The Optional Cancellation Message</span>
                                            </div>
                                            <input type="hidden" name="date_time" value="{{$appointment->date_time}}">
                                            <input type="hidden" name="email" value="{{$appointment->email}}">
                                            <input type="hidden" name="id" value="{{$appointment->id}}">
                                        </div>
                                        <div class="modal-footer border-0">
                                            <button type="submit" class="btn btn-primary">Cancel Appointment</button>
                                            <button type="button" class="btn btn-light" data-dismiss="modal">Back</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 d-inline-block">
                            <hr>
                            <h5>EMAIL</h5>
                            <p>{{$appointment->email}}</p>
                            <p class="text-black-50"><small>Created at {{Carbon\Carbon::parse($appointment->created_at)->format("d M Y")}}</small></p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @else
                <div class="col-md-12">
                    <h2 class="text-center">You Haven't Any Appointment</h2>
                </div>
            @endif
        </div>
    </div>
@endsection