@extends("layouts.myLayout")

@section("content")

    <div class="content">
        <div class="row m-0">
            <div class="col-md-5 mx-auto box-shadow">
                <div class="row">
                    <div class="col-md-4 mx-auto my-4">
                        <img class="img-fluid" src="@if($user->profile_picture === null) {{asset("/storage/images/avatar.jpg")}}
                        @else {{asset($user->profile_picture)}} @endif" alt="Profile Picture">
                    </div>
                    <div class="col-md-7 mt-4">
                        <div class="row justify-content-between">
                            <h6 class="ml-0"><strong>@lang("messages.Name")</strong></h6>
                            <p class="mr-4">{{$user->name}}</p>
                        </div>
                        <hr class="my-2">
                        <div class="row justify-content-between">
                            <h6 class="ml-0"><strong>@lang("messages.Surname")</strong></h6>
                            <p class="mr-4">{{$user->surname}}</p>
                        </div>
                        <hr class="my-2">
                        <div class="row justify-content-between">
                            <h6 class="ml-0"><strong>@lang("messages.Parent Name")</strong></h6>
                            <p class="mr-4">{{$user->parent_name}}</p>
                        </div>
                        <hr class="my-2">
                        <div class="row justify-content-between">
                            <h6 class="ml-0"><strong>@lang("messages.E-mail")</strong></h6>
                            <p class="mr-4">{{$user->email}}</p>
                        </div>
                        <hr>
                        <div class="row justify-content-between">
                            <h6 class="ml-0"><strong>@lang("messages.Phone Number")</strong></h6>
                            <p class="mr-4">{{$user->phone_number}}</p>
                        </div>
                        <hr class="my-2">
                        <div class="row justify-content-between">
                            <h6 class="ml-0"><strong>@lang("messages.Gender")</strong></h6>
                            @if($user->gender === "0")<p class="mr-4">@lang("Male")</p>@else<p class="mr-4">@lang("Female")</p>@endif
                        </div>
                        <hr class="my-2">
                        @if($user->hasrole("doctor"))
                            @if($user->profession !== NULL)
                                <div class="row justify-content-between">
                                    <h6 class="ml-0"><strong>@lang("messages.Profession")</strong></h6>
                                    <p class="mr-4">{{$user->profession}}</p>
                                </div>
                                <hr class="my-2">
                            @endif
                            @if($user->experience !== NULL)
                                <div class="row justify-content-between">
                                    <h6 class="ml-0"><strong>@lang("messages.Experience")</strong></h6>
                                    <p class="mr-4">{{$user->experience}}</p>
                                </div>
                                <hr class="my-2">
                            @endif
                            @if(count($user->certificates) !== 0)
                                <div class="row">
                                    <h6 class="ml-0"><strong>@lang("messages.Certificate")</strong></h6>
                                    <p class="mr-4">{{$user->certificates}}</p>
                                </div>
                                <hr class="my-2">
                            @endif
                            @if(count($user->diplomas) !== 0)
                                @if(count($user->diplomas) === 1)<h6 class="ml-0"><strong>@lang("messages.Diploma")</strong></h6>
                                @else(count($user->diplomas) > 1)<h6 class="ml-0"><strong>@lang("messages.Diplomas")</strong></h6>@endif
                                @foreach($user->diplomas as $diploma)
                                        <div class="col-md-12 my-2">
                                            <img src="{{asset($diploma->path)}}" class="w-100" alt="diploma">
                                        </div>
                                @endforeach
                                <hr class="my-2">
                            @endif
                            @if($user->biography !== NULL)
                                <div class="row justify-content-between">
                                    <h6 class="ml-0"><strong>@lang("messages.Biography")</strong></h6>
                                    <p class="mr-4">{{$user->biography}}</p>
                                </div>
                                <hr class="my-2">
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection