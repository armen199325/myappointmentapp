@extends('layouts.myLayout')

@section('content')
    <div class="content my-4">
        @if(count($doctors) === 0)
            <div class="col-md-12">
                <h2 class="text-center">There aren't Registrated Doctors</h2>
            </div>
        @endif
        @foreach($doctors as $doctor)
            <div class="row  justify-content-around mb-4 mx-0">
                <div class="col-md-8 d-md-flex d-sm-block flex-row bg-white box-shadow rounded">
                    <div class="image d-sm-block col-md-4">
                        <img src='@if($doctor->profile_picture !== null){{asset($doctor->profile_picture)}}
                        @else {{asset("storage/images/avatar.jpg")}} @endif' class="mx-auto col-md-10 col-5 p-0 d-block my-3 box-shadow"  alt="noavatar">
                    </div>
                    <div class="information col-md-8">
                        <h3 class="mt-2 text-center text-md-left"><a class="text-teal" href="{{route("showUser", ["id" => $doctor->id])}}">{{$doctor->name." ".$doctor->surname}}</a></h3>
                        <div class="col-md-12 biography">
                            @if($doctor->biography !== null)
                                <p class="text-justify">{{$doctor->biography}}</p>
                            @endif
                        </div>
                        <hr>
                        <div class="footer">
                            @role("pacient")<a href="{{route("setAppointment", ["id" => $doctor->id])}}" class="material-button link p-1 mb-3 mx-4 float-right">Set Appointment</a>@endrole
                            <p class="text-teal float-right">{{$doctor->experience}}</p>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        {{$doctors->links()}}
    </div>
@endsection
