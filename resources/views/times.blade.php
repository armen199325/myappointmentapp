@extends("layouts.myLayout")

@section("content")
    <input type="hidden" value="{{$date}}" class="current-date">
    <div class="content my-4">
        <div class="row mx-0">
            <p class="mx-auto my-0"><strong>Select a Time</strong></p>
        </div>
        @foreach($dates as $data)
            <div class="row line mx-0 my-3">
                <div class="col-sm-5 mx-auto">
                    <button class="material-button py-2 col-md-12" data-toggle="collapse" data-target="#confirm{{$loop->index}}">
                        {{$data}}
                    </button>
                    <div class="collapse" id="confirm{{$loop->index}}">
                        <div class="row mx-0">
                            <button class="btn btn-dark my-2 col-sm-5 mx-auto" data-toggle="collapse" data-target="#confirm{{$loop->index}}">
                                Cancel
                            </button>
                            <button class="btn btn-primary confirm-button my-2 col-sm-5 mx-auto">
                                Confirm
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection