<!doctype html>
<html class="h-100">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body background=" {{ asset('images/5-noteworthy-blog-design-trends.jpg') }}" style="background-size: cover; background-repeat: no-repeat; height: 100%; background-attachment: fixed;">
    @yield("content")
    <script src="{{asset("js/app.js")}}"></script>
    <script src="{{asset("js/authScript.js")}}"></script>
</body>
</html>