<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body class="bg-white">
    <nav class="navbar navbar-white navbar-expand-md bg-white">
        <div class="mx-3 col-2 d-inline-block" >
            <a href="@role('pacient'){{route("home")}} @endrole @role('doctor') {{route("appointments")}} @endrole"><img src="{{asset("storage/images/56d3f7013ac78_thumb900.jpg")}}" class="logo-image" alt="Logo"></a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas text-teal fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-2 w-100">
                <li class="dropdown align-items-center justify-content-center d-flex flex-column">
                    <a href="#" class="dropdown-toggle text-dark d-inline-block align-self-center align-middle my-auto" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Navigate</a>
                    <div class="dropdown-menu box-shadow" aria-labelledby="dropdownMenuButton">
                        @role("doctor")
                            <a class="dropdown-item" href="{{route("setAppointment")}}">Set Free Times</a>
                            <a class="dropdown-item" href="{{route("appointments")}}">My Appointments</a>
                            <a class="dropdown-item" href="{{route("home")}}">Doctors List</a>
                            <a class="dropdown-item" href="{{route("editProfile")}}">Edit Profile</a>
                        @endrole
                        @role("pacient")

                            <a class="dropdown-item" href="{{route("home")}}">Doctors List</a>
                            <a class="dropdown-item" href="{{route("editProfile")}}">Edit Profile</a>
                        @endrole
                    </div>
                </li>
                <li class="nav-item mx-md-3 col-md-8 col-12">
                    <div class="col-md-10 col-12 mt-2 form-group p-0 position-relative">
                        <input type="text" class="col-md-12 form-control material-input search" required>
                        <span class="material-placeholder">Search</span>
                        <div class="dropdown position-absolute w-100 d-none search-result">
                            <div class="col-md-12 search-drop box-shadow rounded">
                                <div class="lds-default loading"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    @auth
                        <div class="mr-md-3 d-inline-block mx-auto p-0 col-12 profile">
                            <div class="col-3 position-relative my-auto mx-auto ">
                                <img class="profile-picture mx-auto d-block rounded-circle" src="@if(Auth::user()->profile_picture !== null){{asset(Auth::user()->profile_picture)}}
                                @else {{asset("storage/images/avatar.jpg")}} @endif" alt="profile picture">
                                <div class="user-attributes position-absolute bg-light">
                                    <div class="row shadow p-0 m-0 ">
                                        <ul class="d-inline-block pl-3">
                                            <li class="d-block logout cursor-pointer my-1">Logout</li>
                                            <li class="d-inline-block ml-1 ml-md-0 mt-1"><a href="{{ route('changelanguage', ["lang" => "en"]) }}">Eng</a></li>
                                            <li class="d-inline-block ml-1 ml-md-0 mt-1"><a href="{{ route('changelanguage', ["lang" => "am"]) }}">Arm</a></li>
                                        </ul>
                                    </div>
                                    <form class="logout-form d-none" action="{{ route('logout') }}" method="POST">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endauth
                </li>
            </ul>
        </div>
    </nav>
    @yield("content")
    <script src="{{asset("js/app.js")}}"></script>
    <script src="{{asset("js/script.js")}}"></script>
</body>
</html>