@extends("layouts.authLayout")

@section("content")
    <div class="container col my-container d-flex flex-column h-100 position-absolute w-100 my-auto">
        <div class="row justify-content-center my-auto d-flex">
            <div class="col-md-5 border mx-auto bg-white shadow rounded">
                <form id="changepassword">
                    @method("PUT")
                    <div class="col-md-8 mx-auto my-4">
                        <h6 class="text-center">@lang("messages.Change Password")</h6>
                    </div>
                    <div class="d-none">
                        <input type="hidden" value="done" id="done1" name="done">
                    </div>
                    <div class="form-group position-relative my-4">
                        <input type="password" name="current_password" id="current_password" class="form-control rounded-0 material-input" required>
                        <span class="material-placeholder">@lang("messages.Current Password") <sup class="text-danger">*</sup></span>
                    </div>
                    <div class="form-group position-relative my-4">
                        <input type="password" name="new_password" id="new_password" class="pas form-control rounded-0 material-input" required>
                        <span class="material-placeholder">@lang("messages.New Password") <sup class="text-danger">*</sup></span>
                    </div>
                    <div class="form-group position-relative my-4">
                        <input type="password" name="new_password_confirmation" id="pas new_password_confirmation" class="form-control rounded-0 material-input" required>
                        <span class="material-placeholder">@lang("messages.New Password Confirmation") <sup class="text-danger">*</sup></span>
                    </div>
                </form>
                <div class="col-md-12 my-4">
                    <button class="btn btn-primary col-md-8 mx-auto submit-ajax d-block">@lang("messages.Change Password")</button>
                </div>
                <div class="col-md-12 my-3">
                    <a href="{{route("editProfile")}}" class="my-link d-block mx-auto text-center"><i class="fas fa-sign-in-alt"></i> @lang("messages.Back To Edit Profile Page")</a>
                </div>
            </div>
        </div>
    </div>
@endsection