@extends("layouts.authLayout")
@section("content")
<div class="container h-auto col w-100 my-4">
    <div class="row">
        <div class="col-md-5 border mx-auto bg-white shadow rounded">
            @include("languageLinks")
            <form action="/register" method="post" id="register" class="registration-form" novalidate>
                @csrf
                <div class="d-none">
                    <input type="hidden" value="done" id="done" name="done">
                </div>

                <div class="col-md-8 mx-auto mb-4">
                    <h6 class="text-center">@lang("messages.Registration")</h6>
                </div>
                <div class="form-group position-relative my-4">
                    <label for="role">@lang("messages.Registrate as")</label>
                    <select name="role" id="role" class="material-select">
                        <option value="3">@lang("messages.Pacient")</option>
                        <option value="2">@lang("messages.Doctor")</option>
                    </select>
                </div>
                <div class="form-group position-relative my-4">
                    <input type="text" name="name" id="name" class="form-control rounded-0 material-input" required>
                    <span class="material-placeholder">@lang('messages.First Name') <sup class="text-danger">*</sup></span>
                </div>
                <div class="form-group position-relative my-4">
                    <input type="text" name="surname" id="surname" class="form-control rounded-0 material-input" required>
                    <span class="material-placeholder">@lang("messages.Last Name")<sup class="text-danger">*</sup></span>
                </div>
                <div class="form-group position-relative my-4">
                    <input type="text" name="parent_name" id="parent_name" class="form-control rounded-0 material-input" required>
                    <span class="material-placeholder">@lang("messages.Parent Name") <sup class="text-danger">*</sup></span>
                </div>
                <div class="form-group position-relative my-4">
                    <input type="text" name="email" id="email" class="form-control rounded-0 material-input" required>
                    <span class="material-placeholder">@lang("messages.E-mail") <sup class="text-danger">*</sup></span>
                </div>
                <div class="form-group position-relative my-4">
                    <input type="text" name="phone_number" id="phone_number" class="form-control rounded-0 material-input" required>
                    <span class="material-placeholder">@lang("messages.Phone Number") <sup class="text-danger">*</sup></span>
                </div>
                <div class="form-group position-relative my-4">
                    <label for="gender">@lang("messages.Gender")</label>
                    <select name="gender" id="gender" class="material-select">
                        <option value="0">@lang("messages.Male")</option>
                        <option value="1">@lang("messages.Female")</option>
                    </select>
                </div>
                <div class="form-group position-relative my-4 birthday">
                    <label for="birthday">@lang("messages.Birthday")</label>
                    <input type="date" name="birthday" id="birthday" value="1990-01-01" class="form-control rounded-0 material-input d-block material-select" required>
                </div>
                <div class="form-group position-relative my-4">
                    <input type="password" name="password" id="password" class="pas form-control rounded-0 material-input" required>
                    <span class="material-placeholder">@lang("messages.Password") <sup class="text-danger">*</sup></span>
                </div>
                <div class="form-group position-relative my-4">
                    <input type="password" name="password_confirmation" id="password_confirmation" class="pas form-control rounded-0 material-input" required>
                    <span class="material-placeholder">@lang("messages.Password Confirmation") <sup class="text-danger">*</sup></span>
                </div>
                <div class="doctor-role-inputs d-none">
                    <div class="form-group position-relative my-4">
                        <input type="text" id="profession" name="profession" class="form-control rounded-0 material-input" required>
                        <span class="material-placeholder">@lang('messages.Profession') <sup class="text-danger">*</sup></span>
                    </div>
                </div>
                <div class="col-md-12 h-auto">
                    <div class="checkbox h-auto">
                        <input type="checkbox" id="check" class="material-checkbox" name="Terms_and_Conditions">
                        <label class="container-check" for="check"><a href="#" class="my-link">@lang("messages.Terms And Conditions")</a></label>
                    </div>
                </div>
                <div class="col-md-12">
                    <button class="btn btn-primary col-md-8 mx-auto submit-button d-block">@lang("messages.Sign Up Free")<div class="lds-dual-ring-button d-none"></div></button>
                </div>
            </form>
            <div class="col-md-12 my-3">
                <a href="{{route("login")}}" class="my-link d-block mx-auto text-center"><i class="fas fa-sign-in-alt"></i> @lang("messages.Have an Account?")</a>
            </div>
        </div>
    </div>
</div>
@endsection