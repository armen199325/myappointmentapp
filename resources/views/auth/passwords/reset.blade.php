@extends("layouts.authLayout")
@section("content")
    <div class="container col my-container position-absolute d-flex flex-column h-100">
        <div class="row justify-content-center my-auto">
            <div class="col-md-5 border mx-auto bg-white shadow rounded">
                @include("languageLinks")
                <form action="" id="new-password" novalidate>
                    @csrf
                    <div class="d-none">
                        <input type="hidden" value="done" id="done" name="done">
                    </div>
                    <div class="d-none">
                        <input type="hidden" name="token" value="{{ $token }}">
                    </div>
                    <div class="col-md-8 mx-auto mb-4">
                        <h6 class="text-center">@lang("messages.Welcome")</h6>
                    </div>
                    <div class="form-group d-none position-relative">
                        <input type="text" value="{{urldecode($email)}}" id="email" name="email" class="form-control rounded-0 material-input" required>
                        <span class="material-placeholder">@lang("messages.E-mail")</span>
                    </div>
                    <div class="form-group position-relative">
                        <input type="password" id="password" name="password" class="pas form-control rounded-0 material-input" required>
                        <span class="material-placeholder">@lang("messages.Password")</span>
                    </div>
                    <div class="form-group position-relative">
                        <input type="password" id="password_confirmation" name="password_confirmation" class="pas form-control rounded-0 material-input" required>
                        <span class="material-placeholder">@lang("messages.Password Confirmation")</span>
                    </div>
                    <button class="btn btn-primary col-md-8 my-4 mx-auto submit-button d-block">@lang("messages.Reset Password")</button>
                </form>
            </div>
        </div>
    </div>

@endsection