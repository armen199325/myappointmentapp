@extends("layouts.authLayout")
@section("content")
    <div class="container col my-container position-absolute d-flex flex-column h-100">
        <div class="row justify-content-center my-auto">
            <div class="col-md-5 border mx-auto bg-white shadow rounded">
                @include("languageLinks")
                <form action="" id="password-reset" class="my-form" novalidate>
                    @csrf
                    <div class="d-none">
                        <input type="hidden" value="done" id="done" name="done">
                    </div>
                    <div class="col-md-8 mx-auto mb-4">
                        <h6 class="text-center">@lang("messages.Reset Password")</h6>
                    </div>
                    <div class="form-group position-relative">
                        <input type="text" id="email" name="email" class="form-control rounded-0 material-input" required>
                        <span class="text-nowrap material-placeholder">@lang("messages.Enter Your E-mail")</span>
                    </div>
                    <button class="btn btn-primary col-md-10 mx-auto submit-button d-block">@lang("messages.Send Password Reset Link")<div class="lds-dual-ring-button d-none"></div></button>
                </form>
                <div class="col-md-12 my-3">
                    <a href="{{route("login")}}" class="my-link d-block mx-auto text-center"><i class="fas fa-sign-in-alt"></i> @lang("messages.Back to Login Page")</a>
                </div>
            </div>
        </div>
    </div>
@endsection