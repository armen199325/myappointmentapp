@extends("layouts.authLayout")
@section("content")
<div class="container col my-container d-flex flex-column h-100 position-absolute w-100 my-auto">
    <div class="row justify-content-center my-auto">
        <div class="col-md-5 border mx-auto bg-white shadow rounded">
            @include("languageLinks")
            <form action="" id="login" novalidate>
                @csrf
                <div class="d-none">
                    <input type="hidden" value="done" id="done" name="done">
                </div>
                <div class="col-md-8 mx-auto mb-4">
                    <h6 class="text-center">@lang("messages.Welcome")</h6>
                </div>
                <div class="form-group position-relative">
                    <input type="text" id="email" name="email" class="form-control rounded-0 material-input" required>
                    <span class="material-placeholder">@lang("messages.E-mail")</span>
                </div>
                <div class="form-group position-relative">
                    <input type="password" id="password" name="password" class="pas form-control rounded-0 material-input" required>
                    <span class="material-placeholder">@lang("messages.Password")</span>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <div class="checkbox remember">
                            <input type="checkbox" id="check" class="material-checkbox" name="remember">
                            <label class="container-check" for="check">@lang("messages.Remember Me")</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button class="btn btn-primary col-md-8 mx-auto submit-button d-block">@lang("messages.Log in")<div class="lds-dual-ring-button d-none"></div></button>
                </div>
            </form>
            <div class="col-md-12 my-3">
                <a href="{{route("password.request")}}" class="my-link d-block mx-auto text-center"><i class="fas fa-lock"></i> @lang("messages.Forgot Password?")</a>
            </div>
            <div class="col-md-12 mx-auto my-3">
                <a href="{{route('register')}}" class="my-link d-block mx-auto text-center"><i class="fas fa-user-plus"></i> @lang("messages.New Here? Create Account")</a>
            </div>
        </div>
    </div>
</div>

@endsection