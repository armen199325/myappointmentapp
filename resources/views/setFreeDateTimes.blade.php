@extends("layouts.myLayout")

@section("content")
    <div class="content my-4">
        <div class="row mx-0">
            <p class="mx-auto my-0"><strong>@lang("messages.Select Your Free Hours")</strong></p>
        </div>
        <form action="" id="my-form">
            <div class="row mx-0 my-2">
                <div class="col-sm-5 mx-auto">
                    <label for="check10" class="w-100 d-block py-2 my-2 text-center @if(in_array("10:00", $dates)) btn-chosen @endif material-button button-toggle">10:00am</label>
                    <input type="checkbox" id="check10" name="10:00" class="d-none" @if(in_array("10:00", $dates)) checked @endif>
                </div>
            </div>
            <div class="row mx-0 my-2">
                <div class="col-sm-5 mx-auto">
                    <label for="check11" class="w-100 d-block py-2 my-2 text-center @if(in_array("11:00", $dates)) btn-chosen @endif material-button button-toggle">11:00am</label>
                    <input type="checkbox" name="11:00" id="check11" class="d-none" @if(in_array("11:00", $dates)) checked @endif>
                </div>
            </div>
            <div class="row mx-0 my-2">
                <div class="col-sm-5 mx-auto">
                    <label for="check12" class="w-100 d-block py-2 my-2 text-center @if(in_array("12:00", $dates)) btn-chosen @endif material-button button-toggle">12:00am</label>
                    <input type="checkbox" id="check12" name="12:00" class="d-none" @if(in_array("12:00", $dates)) checked @endif>
                </div>
            </div>
            <div class="row mx-0 my-2">
                <div class="col-sm-5 mx-auto">
                    <label for="check01" class="w-100 d-block py-2 my-2 text-center @if(in_array("13:00", $dates)) btn-chosen @endif material-button button-toggle">01:00pm</label>
                    <input type="checkbox" id="check01" name="13:00" class="d-none" @if(in_array("13:00", $dates)) checked @endif>
                </div>
            </div>
            <div class="row mx-0 my-2">
                <div class="col-sm-5 mx-auto">
                    <label for="check02" class="w-100 d-block py-2 my-2 text-center @if(in_array("14:00", $dates)) btn-chosen @endif material-button button-toggle">02:00pm</label>
                    <input type="checkbox" name="14:00" id="check02" class="d-none" @if(in_array("14:00", $dates)) checked @endif>
                </div>
            </div>
            <div class="row mx-0 my-2">
                <div class="col-sm-5 mx-auto">
                    <label for="check03" class="w-100 d-block py-2 my-2 text-center @if(in_array("15:00", $dates)) btn-chosen @endif material-button button-toggle">03:00pm</label>
                    <input type="checkbox" name="15:00" id="check03" class="d-none" @if(in_array("15:00", $dates)) checked @endif>
                </div>
            </div>
            <div class="row mx-0 my-2">
                <div class="col-sm-5 mx-auto">
                    <label for="check04" class="w-100 d-block py-2 my-2 text-center @if(in_array("16:00", $dates)) btn-chosen @endif material-button button-toggle">04:00pm</label>
                    <input type="checkbox" id="check04" name="16:00" class="d-none" @if(in_array("16:00", $dates)) checked @endif>
                </div>
            </div>
            <div class="row mx-0 my-2">
                <div class="col-sm-5 mx-auto">
                    <label for="check05" class="w-100 d-block py-2 my-2 text-center @if(in_array("17:00", $dates)) btn-chosen @endif material-button button-toggle">05:00pm</label>
                    <input type="checkbox" name="17:00" id="check05" class="d-none" @if(in_array("17:00", $dates)) checked @endif>
                </div>
            </div>
            <div class="row mx-0 my-2">
                <div class="col-sm-5 mx-auto">
                    <label for="check06" class="w-100 d-block py-2 my-2 text-center @if(in_array("18:00", $dates)) btn-chosen @endif material-button button-toggle">06:00pm</label>
                    <input type="checkbox" name="18:00" id="check06" class="d-none" @if(in_array("18:00", $dates)) checked @endif>
                </div>
            </div>
            {{--@php $now = Carbon\Carbon::today("10:00") @endphp--}}
            {{--@for($i = 1; $i < 9; $i ++)--}}
                {{--<div class="row mx-0 my-2">--}}
                    {{--<div class="col-sm-5 mx-auto">--}}
                        {{--<label for="check06" class="w-100 d-block py-2 my-2 text-center @if(in_array("18:00", $dates)) btn-chosen @endif material-button button-toggle">06:00pm</label>--}}
                        {{--<input type="checkbox" name="18:00" id="check06" class="d-none" @if(in_array("18:00", $dates)) checked @endif>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--@endfor--}}
            <div class="row mx-0 my-2">
                <div class="col-sm-5 mx-auto">
                    <button class="material-button w-100 testing">
                        @lang("messages.Accept Changes")
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection