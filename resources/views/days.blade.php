@extends("layouts.myLayout")

@section("content")
    <div class="content my-3">
        <div class="row w-100 m-0">
            @role("pacient")
                <div class="col">
                    <p class="text-center">Dr. {{$doctor->name." ".$doctor->surname}}</p>
                </div>
            @endrole
        </div>
        <div class="row m-0">
            <div class="col-md-8 flex-wrap d-flex mx-auto justify-content-between">
                @for($i = 0; $i < count($days); $i ++)
                    <div class="col-md-2 px-1 my-2">
                        @role("pacient")<a href="{{route("TimesForPatient", ["doctor_id" => $doctor->id, "date" => Carbon\Carbon::parse($days[$i])->format("Y-m-d")])}}">@endrole
                        @role("doctor")<a href="{{route("SetFreeTimes", ["date" => Carbon\Carbon::parse($days[$i])->format("Y-m-d")])}}">@endrole
                            <div class="circle-date text-dark">
                                <p class="text-center mt-3 mb-1"><strong>{{$days[$i]}}</strong></p>
                                <p class="text-center mb-0">{{Carbon\Carbon::parse($days[$i])->format("M d")}}</p>
                                <p class="text-center">{{Carbon\Carbon::parse($days[$i])->format("Y")}}</p>
                            </div>
                            </a>
                    </div>
                @endfor
            </div>
        </div>
    </div>
@endsection