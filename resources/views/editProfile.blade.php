@extends("layouts/authLayout")

@section("content")

    <div class="content my-4">
        <div class="row m-0">
            <div class="col-md-5 border mx-auto bg-white shadow rounded">
                @include("languageLinks")
                <form  method="post" enctype="multipart/form-data" id="editprofile" class="registration-form" novalidate>
                    <div class="d-hidden">
                        <input type="hidden" name="done" value="done">
                    </div>
                    <div class="col-md-8 mx-auto py-3">
                        <img src="@if(Auth::user()->profile_picture !== null){{asset(Auth::user()->profile_picture)}}
                                @else {{asset("storage/images/avatar.jpg")}} @endif" class="w-100" id="profile-picture" alt="Profile Picture">
                    </div>
                    @method('PUT')
                    <div class="form-group position-relative mt-4">
                        <label for="profile-picture">@lang("messages.Change Profile Picture")</label>
                        <input type="file" value="@lang("Choose The New Profile Picture")" name="profile_picture" class="file form-control-file" id="profile_picture">
                    </div>
                    <div class="form-group position-relative mt-4">
                        <input type="text" id="name" value="{{Auth::user()->name}}" name="name" class="form-control rounded-0 material-input" required>
                        <span class="material-placeholder">@lang("messages.First Name")</span>
                    </div>
                    <div class="form-group position-relative">
                        <input type="text" id="surname" name="surname" value="{{Auth::user()->surname}}" class="form-control rounded-0 material-input" required>
                        <span class="material-placeholder">@lang("messages.Last Name")</span>
                    </div>
                    <div class="form-group position-relative">
                        <input type="text" id="parent_name" name="parent_name" value="{{Auth::user()->parent_name}}" class="form-control rounded-0 material-input" required>
                        <span class="material-placeholder">@lang("messages.Parent Name")</span>
                    </div>
                    <div class="form-group position-relative">
                        <input type="text" id="phone_number" name="phone_number" value="{{Auth::user()->phone_number}}" class="form-control rounded-0 material-input" required>
                        <span class="material-placeholder">@lang("messages.Phone Number")</span>
                    </div>
                    <div class="form-group position-relative my-4">
                        <label for="gender">@lang("messages.Gender")</label>
                        <select name="gender" id="gender" class="material-select">
                            <option @if(Auth::user()->gender === 0) selected @endif value="0">@lang("messages.Male")</option>
                            <option @if(Auth::user()->gender === 1) selected @endif value="1">@lang("messages.Female")</option>
                        </select>
                    </div>
                    <div class="form-group position-relative my-4">
                        <label for="birthday">@lang("messages.Birthday")</label>
                        <input type="date" name="birthday" id="birthday" value="1990-01-01" class="form-control rounded-0 material-input d-block material-select" required>
                    </div>
                    @role("doctor")
                        <div class="form-group position-relative my-4">
                            <input type="text" id="profession" name="profession" value="{{Auth::user()->profession}}" class="form-control rounded-0 material-input" required>
                            <span class="material-placeholder">@lang('messages.Profession') <sup class="text-danger">*</sup></span>
                        </div>
                        <div class="form-group position-relative my-4">
                            <input type="text" id="experience" name="experience" value="{{Auth::user()->experience}}" class="form-control rounded-0 material-input" required>
                            <span class="material-placeholder">@lang('messages.Experience') <sup class="text-danger">*</sup></span>
                        </div>
                        <div class="form-group position-relative my-4">
                            <input type="text" id="diploma" name="biography" value="{{Auth::user()->biography}}" class="form-control rounded-0 material-input" required>
                            <span class="material-placeholder">@lang('messages.Biography')</span>
                        </div>
                        <div class="form-group position-relative mt-4">
                            <label for="diploma">@lang("messages.Add Diplomas")</label>
                            <input type="file" name="diploma" value="diploma" class="form-control-file" id="diplomas" multiple>
                        </div>
                        @foreach($diplomas as $diploma)
                            <div class="col-md-8 position-relative mx-auto py-3">
                                <img src="{{$diploma->path}}" alt="diploma" type="diploma" class="w-100" identificator="{{$diploma->id}}">
                                <span class="material-placeholder"></span>
                                <span href="" class="fa-trash-alt fa position-absolute delete-link cursor-pointer text-teal"></span>
                            </div>
                        @endforeach
                        <div class="form-group position-relative mt-4">
                            <label for="certificate">@lang("messages.Add Certificates")</label>
                            <input type="file" name="certificate" class="form-control-file" id="certificate" multiple>
                        </div>
                        @foreach($certificates as $certificate)
                            <div class="col-md-8 mx-auto position-relative py-3">
                                <img src="{{$certificate->path}}" alt="certificate" type="certificate" class="w-100" identificator="{{$certificate->id}}">
                                <span class="material-placeholder"></span>
                                <span href="" class="cursor-pointer fa-trash-alt fa position-absolute delete-link text-teal"></span>
                            </div>
                        @endforeach
                    @endrole
                    <div class="col-md-12 my-4">
                        <button class="btn btn-primary col-md-8 mx-auto submit-ajax d-block">@lang("messages.Save Information")</button>
                    </div>
                    <div class="col-md-12 my-3">
                        <a href="{{route("changePassword")}}" class="my-link d-block mx-auto text-center"><i class="fas fa-sign-in-alt"></i> @lang("messages.Change Password")</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection