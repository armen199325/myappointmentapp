{{--<div class="row justify-content-end">--}}
    {{--<div class="col-3 col-sm-2 "><a href="{{ route('changelanguage', ["lang" => "en"]) }}"><img class="w-50 flags" src="{{asset("storage/images/us.png")}}" alt="en"></a></div>--}}
    {{--<div class="col-3 col-sm-2 "><a href="{{ route('changelanguage', ["lang" => "am"]) }}"><img class="w-50 flags" src="{{asset("storage/images/am.png")}}" alt="am"></a></div>--}}
{{--</div>--}}
<div class="row justify-content-before justify-content-md-end">
    <div class="dropdown">
        <button class="btn material-button dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{app()->getLocale()}}
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <ul class="list-group">
                <li class="list-group-item"><a href="{{ route('changelanguage', ["lang" => "en"]) }}"><img class="w-25 flags" src="{{asset("storage/images/us.png")}}" alt="en"> English</a></li>
                <li class="list-group-item"><a href="{{ route('changelanguage', ["lang" => "am"]) }}"><img class="w-25 flags" src="{{asset("storage/images/am.png")}}" alt="am"> Հայերեն</a></li>
            </ul>
        </div>
    </div>
</div>