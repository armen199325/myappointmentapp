<?php

namespace App\Listeners;

use App\Events\onCancelAppointmentEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Mail\CancelAppointment;

class CancelAppointmentListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onCancelAppointmentEvent  $event
     * @return void
     */
    public function handle(onCancelAppointmentEvent $event)
    {
        //
        Mail::to($event->request->input("email"))
            ->queue(new CancelAppointment($event->request->input("date_time"), $event->request->input("message")));
    }
}
