<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use App\Notifications\MailResetPasswordToken;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoleAndPermission;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    const DOCTOR_ID = 2;
    const PAGINATION_NUMBER = 4;
    protected $fillable = [
        'name', 'email', 'password',"surname", "parent_name", "gender", "birthday", "phone_number", "profession",
        "experience", "certificate", "diploma", "profile_picture","biography",
    ];
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordToken($token));
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function patientAppointments(){
        return $this->hasMany("App\Appointment", "patient_id", "id");
    }
    public function doctorAppointments(){
        return $this->hasMany("App\Appointment", "doctor_id", "id");
    }
    public function diplomas(){
        return $this->hasMany("App\Diploma", "user_id", "id");
    }
    public function certificates(){
        return $this->hasMany("App\Certificate", "user_id", "id");
    }
}
