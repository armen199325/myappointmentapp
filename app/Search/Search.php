<?php

namespace App\Search;

use App\User;

class Search
{
    public static function searchUser($request){
        $searching = $request->input("name");
        $result = self::searchByOneWord($searching);
        if(count($result) === 0) {
            $arrSearch = explode(" ", $searching);
            if(count($arrSearch) > 1){
                $users = self::searchByMoreWords($arrSearch);
                $result = self::getUnuniqueValues($users, count($arrSearch));
            }
        }
        return $result;
    }
    protected static function searchByOneWord($searching){
        $user = User::where("name", 'LIKE', '%'.$searching . '%')
            ->orWhere("surname", 'LIKE', '%' . $searching . '%')
            ->orWhere("parent_name", 'LIKE', '%' . $searching . '%')
            ->orWhere("email", 'LIKE', '%' . $searching . '%')
            ->get();
        return $user;
    }
    protected static function searchByMoreWords($arrSearch){
        $users = [];
        for($i = 0; $i < count($arrSearch); $i ++){
            $users = array_merge($users, User::where("name", 'LIKE', '%'.$arrSearch[$i] . '%')
                ->orWhere("surname", 'LIKE', '%' . $arrSearch[$i] . '%')
                ->orWhere("parent_name", 'LIKE', '%' . $arrSearch[$i] . '%')
                ->orWhere("email", 'LIKE', '%' . $arrSearch[$i] . '%')
                ->get()->toArray());
        }
        return $users;
    }
    protected static function getUnuniqueValues($user, $count){
        $newArray = array_count_values(array_map("serialize", $user));
        $result = [];
        foreach($newArray as $key => $value){
            if($value === $count){
                $result[] = unserialize($key);
            }
        }
        return $result;
    }
}