<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class CancelAppointment extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $date_time;
    protected $myMessage;
    public function __construct($date_time, $myMessage)
    {
        //
        $this->date_time = $date_time;
        $this->myMessage = $myMessage;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('cancelMessage', ["user" => Auth::user(), "date_time" => $this->date_time,
            "myMessage" => $this->myMessage]);
    }
}
