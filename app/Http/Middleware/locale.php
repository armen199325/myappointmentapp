<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->cookie("locale") !== NULL){
            if(in_array($request->cookie("locale") , config("app.languages"))){
                app()->setLocale($request->cookie("locale"));
            }
        } else {
            app()->setLocale(config("app.locale"));
        }
        return $next($request);
    }
}
