<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware("locale");
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $date = Carbon::now()->subYears(18)->format("Y-m-d");
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'parent_name' => 'required|string|max:255',
            "phone_number"=>"required|min:8|max: 20|regex: /^[0-9\+]{1,}[0-9\-]{3,15}$/",
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            "birthday" => "date|required|before:$date",
            "Terms_and_Conditions" => "accepted",
            "role"=>"required|numeric|between:2,3",
            "gender"=>"required|numeric|between:0,1",
            "profession"=>"max:255|required_if:role,2",
            "done"=>"required",
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $create_user = [
            'name' => $data['name'],
            'surname' => $data['surname'],
            'parent_name' => $data['parent_name'],
            'gender'=>$data['gender'],
            'birthday' => $data['birthday'],
            'phone_number' => $data['phone_number'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ];
        if($data["role"] == User::DOCTOR_ID){
            $create_user = array_merge($create_user, [
                "profession"=>$data["profession"],
            ]);
        }
        $user = User::create($create_user);
        $user->attachRole($data["role"]);
        return $user;
    }
    protected function registered(){
        return [
            "status" => "success",
            "redirectURL" => $this->redirectTo(),
        ];
    }
    protected function redirectTo(){
        return "/login";
    }
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }
}
