<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Appointment;
use Event;
use Mail;
use App\Events\onCancelAppointmentEvent;
use Carbon\Carbon;

class AppointmentController extends Controller
{
    //
    public function show($id = 0){
        if(Auth::user()->hasRole("pacient") && $id === 0){
            abort(404);
        }
        $doctor = User::find($id);
        $now = Carbon::now();
        $days = [];
        for($i = 1; $i < 8; $i ++){
            if($now->isWeekday()){
                $days[] = $now->format("l");
            }
            $now->addDay(1);
        }
        return view("days", ["doctor" => $doctor, "days" => $days]);
    }
    public function showTime($doctor_id, $date ){
        $thisDay = Carbon::parse($date);
        $nextDay = Carbon::parse($thisDay)->addDay(1);
        $datas = Appointment::where("date_time", ">", $thisDay)->where("date_time", "<", $nextDay)->where("state", "0")
            ->where("doctor_id", $doctor_id)->orderBy("date_time")->get(["date_time"]);
        if(count($datas) >= 1){
            for($i = 0; $i < count($datas); $i++){
                $dates[] = Carbon::parse($datas[$i]["date_time"])->format("H:i");
            };
        } else {
            $dates = [];
        }
        return view("times", ["dates"=> $dates, "date" => $date]);
    }
    public function store($doctorId, Request $request){
        $doctor = User::find($doctorId);
        $date = Carbon::parse($request->input("date")." ".$request->input("time"));
        $appointment = Appointment::where("date_time",  $date)
            ->where("doctor_id", $doctorId)->first();
        $appointment->patient_id = Auth::id();
        $appointment->state = "1";
        $appointment->save();
        return __("messages.The Appointment With Dr. ").$doctor->name." ". $doctor->surname .__("messages. Has Been Set. Thank You");
    }
    public function setFreeDatetimes($date){
//        dump(Carbon::today());
        $thisDay = Carbon::parse($date);
        $nextDay = Carbon::parse($date)->addDay(1);
        $date = Appointment::where("date_time", ">", $thisDay)->where("date_time", "<", $nextDay)->get(["date_time"]);
        for($i = 0; $i < count($date); $i++){
            $dates[] = date("H:i",strtotime($date[$i]["date_time"]));
        }
        if(!isset($dates)){
            $dates = [];
        }
        return view("setFreeDateTimes", ["dates" => $dates]);
    }
    public function storeFreeDatetimes(Request $request){
        $thisDay = Carbon::parse($request->input("date"));
        $nextDay = Carbon::parse($request->input("date"))->addDay(1);
        $date = Appointment::where("date_time", ">", $thisDay)->where("date_time", "<", $nextDay)
            ->where("doctor_id", Auth::id())->get(["date_time"]);
        if(count($date) !== 0){
            for($i = 0; $i < count($date); $i++){
                $dates[] = Carbon::parse($date[$i]["date_time"])->format("H:i");
            };
        } else {
            $dates = [];
        }
        $data = [];
        parse_str($request->time, $data);
        foreach($data as $myKey => $value){
            $key = array_search($myKey, $dates);
            if($key !== false){
                unset($date[$key]);
            } else {
                $appointment = new Appointment;
                $appointment->doctor_id = Auth::id();
                $appointment->date_time = $request->date." $myKey:00";
                $appointment->save();
            }
        }
        foreach($date as $key => $value){
            $deletingAppointments = Appointment::where("date_time", $value["date_time"])->where("doctor_id", Auth::id())->first();
            if($deletingAppointments["state"] !== 1){
                $deletingAppointments->delete();
            } else {
                return __("You Can't unset "). $deletingAppointments->date_time. __(" Free Time, Because You Have an Appointment on "). $deletingAppointments->date_time;
            }
        }
        return __("messages.Free Hours Are Set");
    }
    public function showMyAppointments(){
        $appointments = Appointment::where("doctor_id", Auth::id())->where("state", "1")
            ->where("date_time", ">", Carbon::now())
            ->join("users", "appointments.patient_id", "=", "users.id")
            ->orderBy("date_time")
            ->get(["appointments.id", "patient_id", "name", "surname", "date_time", "email", "appointments.created_at"]);
        if(count($appointments) === 0){
            $appointments = [];
        }
        $oldWeekDay = "";
        return view("appointments", ["appointments" => $appointments, "oldWeekDay" => $oldWeekDay]);
    }
    public function cancelAppointment(Request $request){
        $appointment = Appointment::find($request->input("id"));
        $appointment->state = 0;
        $appointment->patient_id = null;
        $appointment->save();
        Event::fire(new onCancelAppointmentEvent($request));
        return back();
    }
}
