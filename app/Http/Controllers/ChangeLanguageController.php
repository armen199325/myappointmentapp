<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChangeLanguageController extends Controller
{
    public function change($lang){
        return back()->cookie("locale", $lang, 45000);
    }
}
