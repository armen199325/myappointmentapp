<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use jeremykenedy\LaravelRoles\Models\Role;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctors = Role::find(User::DOCTOR_ID)->users()->paginate(User::PAGINATION_NUMBER);
//        $doctors = DB::table('role_user')
//            ->join("users", "role_user.user_id", "=", "users.id")
//            ->where("role_user.role_id", "2")->get();
        return view('home', ["doctors"=> $doctors,]);
    }
}
