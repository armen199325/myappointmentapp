<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EditProfileRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use App\User;
use App\Search\Search;
use App\Diploma;
use App\Certificate;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    public function edit(){
        $certificates = Auth::user()->certificates;
        $diplomas = Auth::user()->diplomas;
        return view("editProfile", ["certificates" => $certificates, "diplomas" => $diplomas]);
    }
    public function change(){
        return view("changePassword");
    }
    public function changePassword(Request $request){
        if(Hash::check($request->input("current_password"), Auth::user()->password)){
            $request->validate([
                "new_password" => "required|min:6|confirmed",
                "done" => "required",
            ]);
            $user = Auth::user();
            $user->password = bcrypt($request->input("new_password"));
            $user->save();
            return __("messages.The Password Has Been Changed");
        } else {
            throw ValidationException::withMessages([
                "current_password"=> __('messages.The Current Password is Wrong'),
            ]);
        }
    }
    public function store(EditProfileRequest $request){
        $user = Auth::user();
        if($request->hasFile("profile_picture")){
            $oldPicture = Auth::user()->profile_picture;
            $name = time() . Auth::user()->email . $request->profile_picture->getClientOriginalName();
            $request->profile_picture->move(public_path('storage/images'), $name);
            $myRequest = $request->all();
            $myRequest["profile_picture"] = "storage/images/" .$name;
            $user->update($myRequest);
            if($oldPicture !== null){
                unlink($oldPicture);
            }
        }
        if ($request->hasFile("diploma")){
            for($i = 0; $i < count($request->diploma); $i++){
                $diploma = new Diploma;
                $name = time() . Auth::user()->email . $request->diploma[$i]->getClientOriginalName();
                $request->diploma[$i]->move(public_path('storage/images'), $name);
                $diploma->user_id = Auth::id();
                $diploma->path = "storage/images/".$name;
                $diploma->save();
            }
        }
        if($request->hasFile("certificate")){
            for($i = 0; $i < count($request->certificate); $i ++){
                $certificate = new Certificate;
                $name = time() . Auth::user()->email . $request->certificate[$i]->getClientOriginalName();
                $request->certificate[$i]->move(public_path('storage/images'), $name);
                $certificate->user_id = Auth::id();
                $certificate->path = "storage/images/".$name;
                $certificate->save();
            }
        }
        $update = [
            "name" => $request->input("name"),
            "surname" => $request->input("surname"),
            "parent_name" => $request->input("parent_name"),
            "phone_number" => $request->input("phone_number"),
            "gender" => $request->input("gender"),
            "birthday" => $request->input("birthday"),
        ];
        if(Auth::user()->hasRole("doctor")){
            $update = array_merge($update, [
                "profession" => $request->input("profession"),
                "experience" => $request->input("experience"),
                "biography" => $request->input("biography"),
            ]);
        }
        $user->update($update);
        return __("messages.The Profile Has Been Edited");
    }
    public function show($id){
        $user = User::find($id);
        return view("userPage", ["user" => $user]);
    }
    public function search(Request $request){
        return Search::searchUser($request);
    }
    public function delete(Request $request){
        if($request->input("type") === "diploma"){
            $diploma = Diploma::find($request->input("id"));
            if(Gate::allows("delete-post", $diploma)){
                unlink($diploma->path);
                $diploma->delete();
            } else {
                return "You Haven't Permission To Delete This Post";
            }
        } elseif ($request->input("type") === "certificate") {
            $certificate = Certificate::find($request->input("id"));
            if(Gate::allows("delete-post", $certificate)){
                unlink($certificate->path);
                $certificate->delete();
            } else {
                return "You Haven't Permission To Delete This Post";
            }
        }
        $type = ucfirst($request->input("type"));
        return "The $type is Deleted";
    }
}
