<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $date = date("Y-m-d");
        return [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'parent_name' => 'required|string|max:255',
            "phone_number"=>"required|min:8|max: 20|regex: /^[0-9\+]{1,}[0-9\-]{3,15}$/",
            "birthday" => "date|required|before:$date",
            "gender"=>"required|numeric|between:0,1",
            "experience"=>"max:255|required_if:role,2",
            "profession"=>"max:255|required_if:role,2",
            "done"=>"required",
            "profile_picture" => "image|max:2000",
            "diploma.*" => "image|max:2000",
            "certificate.*" => "image|max:2000",
            "biography" => "max:300",
        ];
    }
}
