/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 39);
/******/ })
/************************************************************************/
/******/ ({

/***/ 39:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(40);


/***/ }),

/***/ 40:
/***/ (function(module, exports) {

myApp = {
    eventBind: function eventBind() {
        var _this = this;

        this.time = null;
        this.date = null;
        this.reader = null;
        this.delay = null;
        $(".toggle-icon").on("click", function () {
            $(this).children().toggleClass("fa-times fa-angle-up");
        });
        $(".confirm-button").on("click", function (event) {
            _this.time = $(event.target).parent().parent().siblings("button").text();
            _this.date = location.pathname.split("/")[3];
            myApp.confirmAppointment(_this.time, _this.date, event.target);
        });
        $(".testing").on("click", function (event) {
            event.preventDefault();
            _this.times = $("#my-form").serialize();
            _this.date = location.pathname.split("/")[2];
            myApp.confirmAppointment(_this.times, _this.date);
        });
        $(".button-toggle").on("click", function () {
            $(this).toggleClass("btn-chosen");
        });
        $(".search").on("keyup", function (event) {
            if ($(event.target).val() === "") {
                return;
            }
            clearTimeout(_this.delay);
            _this.delay = setTimeout(function () {
                myApp.search(event.target);
            }, 1000);
        });
        $(window).on("click", function () {
            if (!$(this).is(".search-result", ".search-drop", ".searching")) {
                $(".search-result").addClass("d-none");
            }
        });
        $(".logout").on("click", function () {
            $(".logout-form").submit();
        });
    },
    search: function search(input) {
        var searchdrop = $(".search-drop");
        var resultDiv = $(".search-result");
        resultDiv.removeClass("d-none");
        searchdrop.html('<div class="mx-auto d-block py-1 lds-dual-ring"></div>');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "/search",
            type: "GET",
            data: {
                name: $(input).val()
            },
            success: function success(data) {
                searchdrop.html("");
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        searchdrop.append("\n                            <a class=\"d-block text-teal searching my-2\" href=\"/user/" + data[i].id + "\">" + (data[i].name + " " + data[i].surname) + "</a>\n                        ");
                    }
                } else {
                    searchdrop.append("<span class='d-block text-teal searching my-2'>0 Result</span>");
                }
            },
            error: function error(data) {
                console.log(data);
            }
        });
    },
    confirmAppointment: function confirmAppointment(time, date) {
        var e = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: location.pathname,
            type: "POST",
            data: {
                date: date,
                time: time,
                _method: "PUT"
            },
            success: function success(data) {
                console.log(data);
                alert(data);
                if (e !== null) {
                    $(e).parentsUntil(".content").remove();
                }
            },
            error: function error(data) {
                console.log(data);
            }
        });
    },
    init: function init() {
        this.eventBind();
    }
};
myApp.init();

/***/ })

/******/ });