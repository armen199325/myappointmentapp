/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 37);
/******/ })
/************************************************************************/
/******/ ({

/***/ 37:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(38);


/***/ }),

/***/ 38:
/***/ (function(module, exports) {

var myApp = {
    CurrentInformation: {},
    Registration: {
        loading: false,
        submitting: false,
        form: $(".registration-form"),
        reader: null,
        eventBind: function eventBind() {
            var _this = this;

            $("input, select").on("focusout", function (event) {
                if ($(event.target).val() === "") {
                    return;
                }
                _this.sendRequest(event.target);
            });
            $(".submit-ajax").on("click", function (e) {
                e.preventDefault();
                myApp.Registration.submitAjax();
            });
            $("input.file").on("change", function (event) {
                if (event.target.files) {
                    console.log(event.target);
                    _this.reader = new FileReader();
                    _this.reader.onload = function (e) {
                        $("#profile-picture").attr("src", e.target.result);
                    };
                    _this.reader.readAsDataURL(event.target.files[0]);
                }
            });
            $(".checkbox").not(".remember").on("change", function (event) {
                _this.sendRequest(event.target);
            });
            $("#role").change(function () {
                if ($("#role").val() === "2") {
                    $(".doctor-role-inputs").removeClass("d-none");
                } else {
                    $(".doctor-role-inputs").addClass("d-none");
                }
            });
            $(".submit-button").on("click", function (event) {
                event.preventDefault();
                _this.submitForm();
            });
            $(".delete-link").on("click", function () {
                myApp.Registration.deletePost(this);
            });
        },
        deletePost: function deletePost(link) {
            var id = void 0;
            var image = void 0;
            var type = void 0;
            image = $(link).siblings("img");
            id = image.attr("identificator");
            type = image.attr("type");
            $.ajax({
                type: "POST",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url: "/deletepost",
                data: {
                    _method: "DELETE",
                    id: id,
                    type: type
                },
                success: function success(data) {
                    $(link).parent().remove();
                    alert(data);
                },
                error: function error(data) {
                    console.log(data);
                }
            });
        },
        submitAjax: function submitAjax() {
            var errMessages = void 0;
            var data = void 0;
            myApp.Registration.getIdAndUri();
            data = new FormData(document.getElementById(this.id));
            data.append("done", "done");
            console.dir(data.getAll("diploma"));
            for (var i = 0; i < document.getElementById("diplomas").files.length; i++) {
                data.append("diploma[]", document.getElementById("diplomas").files[i]);
            }
            for (var _i = 0; _i < document.getElementById("certificate").files.length; _i++) {
                data.append("certificate[]", document.getElementById("certificate").files[_i]);
            }
            $.ajax({
                type: "POST",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url: "/" + this.uri,
                processData: false,
                contentType: false,
                data: data,
                success: function success(data) {
                    alert(data);
                },
                error: function error(data) {
                    console.log(data);
                    errMessages = data.responseJSON.errors;
                    myApp.Registration.showErrorMessages(errMessages);
                }
            });
        },
        getIdAndUri: function getIdAndUri() {
            this.uri;
            this.id;
            if (location.pathname.replace(/\//g, '') !== "passwordreset" && location.pathname.replace(/\//g, '').length < 15) {
                this.uri = location.pathname.replace(/\//g, '');
                this.id = this.uri;
            } else if (location.pathname.length > 15) {
                this.uri = "password/reset";
                this.id = "new-password";
            } else {
                this.uri = "password/email";
                this.id = "password-reset";
            }
        },
        sendRequest: function sendRequest(thisInput) {
            var errMessages = void 0;
            myApp.Registration.getIdAndUri();
            $.ajax({
                type: "POST",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url: "/" + this.uri,
                data: $("#" + this.id + " :input[name!=done]").serialize(),
                success: function success(data) {
                    if (data.errors !== undefined) {
                        errMessages = data.errors;
                    }
                },
                error: function error(data) {
                    if (myApp.Registration.submitting) {
                        return;
                    }
                    myApp.Registration.errorMessageHandler(data, thisInput);
                }
            });
        },
        errorMessageHandler: function errorMessageHandler(data, thisInput) {
            console.log(data);
            errMessages = data.responseJSON.errors;
            console.log(errMessages);
            if ($(thisInput).attr("name") !== "Terms_and_Conditions") {

                myApp.Registration.showValidationMessages(thisInput, errMessages);
            } else {
                myApp.Registration.showTermsValidationMessage($(thisInput).parent(), errMessages);
            }
        },
        removeConfirmationMessage: function removeConfirmationMessage(errMessages) {
            var thisInput = $("#password");
            if (errMessages[thisInput.attr("name")] === undefined || $.inArray("The password confirmation does not match.", errMessages[thisInput.attr("name")]) === -1) {
                var removeMessage = $("#password_confirmation");
                removeMessage.nextUntil(".material-placeholder").slideUp("normal", function () {
                    $(this).remove(".invalid");
                });
                removeMessage.parent().removeClass("invalid");
            }
        },
        showValidationMessages: function showValidationMessages(thisInput, errMessages) {
            var changeInput = false;
            myApp.Registration.removeConfirmationMessage(errMessages);
            $(thisInput).nextUntil(".material-placeholder").slideUp("normal", function () {
                $(this).remove();
            });
            if (thisInput.name === "password_confirmation") {
                thisInput = document.getElementById("password");
                changeInput = true;
            }
            if (errMessages[thisInput.name] !== undefined) {
                for (var i = 0; i < errMessages[thisInput.name].length; i++) {
                    if (errMessages[thisInput.name][i] === "The password confirmation does not match.") {
                        if (errMessages[thisInput.name].length === 1) {
                            $(thisInput).parent().removeClass("invalid");
                        }
                        if (!changeInput) {
                            continue;
                        }
                        myApp.Registration.showMessage(document.getElementById("password_confirmation"), errMessages[thisInput.name][i]);
                    } else {
                        myApp.Registration.showMessage(thisInput, errMessages[thisInput.name][i]);
                    }
                }
            } else {
                $(thisInput).siblings("span").not(".material-placeholder").slideUp("normal", function () {
                    $(this).remove(".invalid");
                });
                $(thisInput).parent().removeClass("invalid");
            }
        },
        showMessage: function showMessage(thisInput, message) {
            $(thisInput).nextUntil(".material-placeholder").remove(".invalid");
            $(thisInput).after("<span class=\"invalid d-block\" style=\"display: none!important;\">\n                        <strong>" + message + "<br></strong>\n                        </span>");
            $(thisInput).parent().addClass("invalid");
            $(thisInput).nextUntil(".material-placeholder").slideDown();
        },
        showTermsValidationMessage: function showTermsValidationMessage(thisInput, errMessages) {
            if (errMessages[$(thisInput).children("input").attr("name")] !== undefined) {
                $(thisInput).addClass("invalid");
                $(thisInput).children("span").remove();
                for (var i = 0; i < errMessages[$(thisInput).children("input").attr("name")].length; i++) {
                    $(thisInput).children("label").after("<span class=\"invalid d-block\" style=\"display: none!important;\">\n                        <strong>" + errMessages[$(thisInput).children("input").attr("name")][i] + "</strong>\n                        </span>");
                }
                $(thisInput).children("span").slideDown();
            } else {
                $(thisInput).removeClass("invalid");
                $(thisInput).children("span").slideUp("normal", function () {
                    $(this).remove(".invalid");
                });
            }
        },
        submitForm: function submitForm() {
            if (myApp.Registration.loading) {
                return;
            }
            myApp.Registration.submitting = true;
            $(".lds-dual-ring-button").removeClass("d-none");
            myApp.Registration.loading = true;
            var errMessages = void 0;
            var data = void 0;
            this.getIdAndUri();
            data = new FormData(document.getElementById(this.id));
            data.append("done", "done");
            $.ajax({
                type: "POST",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url: "/" + this.uri,
                processData: false,
                contentType: false,
                data: data,
                success: function success(data) {
                    $(".lds-dual-ring-button").addClass("d-none");
                    console.log(data);
                    if (data.message !== undefined) {
                        alert(data.message);
                    }
                    $(".alert-danger").slideUp();
                    if (data.status === "success" && data.redirectURL !== undefined) {
                        location.replace(data.redirectURL);
                    }
                    myApp.Registration.loading = false;
                    myApp.Registration.submitting = false;
                },
                error: function error(data) {
                    $(".lds-dual-ring-button").addClass("d-none");
                    errMessages = data.responseJSON.errors;
                    myApp.Registration.showErrorMessages(errMessages);
                    myApp.Registration.loading = false;
                    myApp.Registration.submitting = false;
                }
            });
        },
        showErrorMessages: function showErrorMessages(errMessages) {
            $("div.invalid").removeClass("invalid");
            $("span.invalid").remove();
            var thisElement = void 0;
            $.each(errMessages, function (key, value) {
                if (key !== "Terms_and_Conditions") {
                    for (var i = 0; i < value.length; i++) {
                        if (value[i] === "The password confirmation does not match.") {
                            myApp.Registration.showSubmittedMessages($("#password_confirmation"), value[i]);
                        } else if (value[i] === "These credentials do not match our records.") {
                            myApp.Registration.showAlertMessage(value[i]);
                        } else {
                            myApp.Registration.showSubmittedMessages($("#" + key), value[i]);
                        }
                    }
                }
            });
        },
        showSubmittedMessages: function showSubmittedMessages(thisElement, message) {
            thisElement.parent().addClass("invalid");
            thisElement.after("<span class=\"invalid d-block\" style=\"display: none!important;\">\n                    <strong>" + message + "<br></strong>\n                    </span>");
            thisElement.nextUntil(".material-placeholder").slideDown();
        },
        showAlertMessage: function showAlertMessage(message) {
            $("form").before("\n                <div class=\"alert alert-danger\" style=\"display: none\" role=\"alert\">\n                    <p class=\"text-center\">" + message + "</p>\n                </div>\n            ");
            $(".alert-danger").slideDown();
        }
    },
    init: function init() {
        myApp.Registration.eventBind();
    }
};
myApp.init();

/***/ })

/******/ });