<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get("changelanguage/{lang}", ["uses" => "ChangeLanguageController@change", "as"=>"changelanguage", "middleware"=>"locale"]);
Route::get("editprofile", ["uses"=> "UserController@edit", "as"=>"editProfile", "middleware"=> ["locale", "auth"]]);
Route::get("changepassword", ["uses"=>"UserController@change", "as"=>"changePassword", "middleware"=> ["locale", "auth"]]);
Route::put("changepassword", ["uses"=>"UserController@changePassword", "as"=>"changePassword", "middleware"=> ["locale", "auth"]]);
Route::put("editprofile", ["uses"=> "UserController@store", "as"=> "changeProfile", "middleware"=> ["locale", "auth"]]);
Route::get("setappointment/{id?}", ["uses" => "AppointmentController@show", "as" => "setAppointment"]);
Route::get("settime/{doctor_id}/{date}", ["uses"=>"AppointmentController@showTime", "as" => "TimesForPatient" , "middleware" => ["locale", "auth", "role:pacient"]])
    ->where(["doctor_id"=>"[0-9]+", "date" => "^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$"]);
Route::put("settime/{doctor_id}/{date}", ["uses"=> "AppointmentController@store", "middleware" => ["locale", "auth", "role:pacient"]])
    ->where(["doctor_id"=>"[0-9]+", "date" => "^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$"]);
Route::get("setfreetimes/{date}", ["uses"=> "AppointmentController@setFreeDatetimes", "as" => "SetFreeTimes" ,"middleware"=> ["locale", "auth", "role:doctor"]])
    ->where(["date" => "^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$"]);
Route::put("setfreetimes/{date}", ["uses" => "AppointmentController@storeFreeDatetimes", "middleware"=> ["locale", "auth", "role:doctor"]])
    ->where(["date" => "^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$"]);
Route::get("appointments", ["uses" => "AppointmentController@showMyAppointments","as" => "appointments", "middleware"=> ["locale", "auth", "role:doctor"]]);
Route::get("user/{id}", ["uses" => "UserController@show", "as" => "showUser", "middleware"=> ["locale", "auth"]])
    ->where(["id"=>"[0-9]+"]);
Route::put("mail", ["uses" => "AppointmentController@cancelAppointment", "as" => "mail"]);
Route::get("search", ["uses" => "UserController@search", "as" => "search"]);
Route::delete("deletepost", ["uses" => "UserController@delete", "as" => "deletePost", "middleware" => "auth"]);
